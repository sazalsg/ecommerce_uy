<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;
class CategoryController extends Controller
{
    public function addCategory()
    {
        return view('admin.category.add');
    }
    
    public function newCategory(Request $data){
    	// using query builder
    	// DB::table('categories')->insert([

    	// 		'category_name'=>$data->category_name,
    	// 		'category_description'=>$data->category_description,
    	// 		'status'=>$data->status
    	// ]);
    	// query buider end

    	// elequent orm

    	// $category=new Category();
    	// $category->category_name=$data->category_name;
    	// $category->category_description=$data->category_description;
    	// $category->status=$data->status;
    	// $category->save();

    	// return 'success';
// for saving mass data it can be use and need to write fillable or guarded in model class.if used guarded then all the form name must be exist in database column
    	Category::create($data->all());

    	return 'sucess';


    }

    public function manageCategory(){
    	$categories=Category::all();
    	return view('admin.category.manage',['categories'=>$categories]);
    }
}
