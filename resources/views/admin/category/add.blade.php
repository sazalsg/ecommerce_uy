@extends('admin.master')
@section('body')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-body rounded-0">
				<form action="{{route('new-category')}}" method="POST">
					
					@csrf
					<div class="form-group row">
						<label class="col-form-label col-md-3">Category Name</label>
						<div class="col-md-9">
							<input type="text" class="form-control" name="category_name" />
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-3">Category Description</label>
						<div class="col-md-9">
							<textarea name="category_description" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-3">Public Status</label>
						<div class="col-md-9">
							<label><input type="radio" name="status" value="1" checked>Published</label>
							<label><input type="radio" name="status" value="0">unpublished</label>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-3"></label>
						<div class="col-md-9">
							<input type="submit" class="btn btn-success" value="save">
						</div>
					</div>
				</form>
				
			</div>
			
		</div>
	</div>
	
</div>
@endsection
